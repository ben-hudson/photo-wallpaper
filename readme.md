# photo-wallpaper

National Geographic Photo of the Day wallpaper changer. Available for Windows, GNOME, [Android](https://play.google.com/store/apps/details?id=photowallpaper.android) and [BlackBerry](http://appworld.blackberry.com/webstore/content/50694891). There is also a [REST API](http://photo-wallpaper.herokuapp.com/).
