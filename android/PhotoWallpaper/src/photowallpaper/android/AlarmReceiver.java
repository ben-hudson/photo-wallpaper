package photowallpaper.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent incoming) {
		Intent outgoing = new Intent(context, WallpaperService.class);
		context.startService(outgoing);
		Log.d("Alarm Receiver", "Received Alarm");
	}

}
