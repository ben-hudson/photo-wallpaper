package photowallpaper.android;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

class GetImageTask extends AsyncTask<Object, Object, Bitmap> {

	private String url;
	private AsyncTaskListener listener = null;

	public GetImageTask(String url, AsyncTaskListener listener) {
		super();
		this.url = url;
		this.listener = listener;
	}

	@Override
	protected Bitmap doInBackground(Object... arg) {
		InputStream is = null;
		Bitmap bitmap = null;

		try {
			URLConnection urlConnection = new URL(url).openConnection();
			is = urlConnection.getInputStream();
		} catch (MalformedURLException e) {
			Log.e("Malformed URL Exception", e.toString());
		} catch (IOException e) {
			Log.e("IO Exception", e.toString());
		}

		try {
			bitmap = BitmapFactory.decodeStream(is);
			is.close();
		} catch (IOException e) {
			Log.e("IO Exception", e.toString());
		} catch (NullPointerException e) {
			Log.e("Null Pointer Exception", e.toString());
		}

		return bitmap;
	}

	@Override
	protected void onPostExecute(Bitmap bitmap) {
		if (listener != null) {
			listener.onImageTaskRepsonse(bitmap);
		}
	}

}
