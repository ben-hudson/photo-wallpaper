package photowallpaper.android;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class WallpaperService extends Service implements AsyncTaskListener {

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onImageTaskRepsonse(Bitmap bitmap) {
		WallpaperManager manager = WallpaperManager.getInstance(this);
		try {
			manager.setBitmap(bitmap);
			Toast.makeText(this, getString(R.string.set_wallpaper),
					Toast.LENGTH_SHORT).show();
			Log.d("Wallpaper Service", "Set Wallpaper");
		} catch (Exception e) {
			Toast.makeText(this, getString(R.string.error_title),
					Toast.LENGTH_LONG).show();
			Log.e("Exception", e.toString());
		}

		File path = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"Photo of the Day Wallpaper");
		path.mkdirs();
		Calendar today = Calendar.getInstance();
		File file = new File(path, "image" + today.get(Calendar.DAY_OF_WEEK)
				+ ".jpg");
		try {
			file.createNewFile();
			FileOutputStream stream = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			stream.close();
		} catch (FileNotFoundException e) {
			Log.e("File Not Found Exception", e.toString());
		} catch (IOException e) {
			Log.e("IO Exception", e.toString());
		} catch (NullPointerException e) {
			Log.e("Null Pointer Exception", e.toString());
		}
	}

	@Override
	public void onJSONTaskResponse(JSONObject jObj) {
		WallpaperManager manager = WallpaperManager.getInstance(this);
		Point size = new Point(manager.getDesiredMinimumWidth(),
				manager.getDesiredMinimumHeight());
		try {
			new GetImageTask(jObj.getString("link") + size.x + "x" + size.y
					+ ".jpg", this).execute();
		} catch (JSONException e) {
			Toast.makeText(this, getString(R.string.error_title),
					Toast.LENGTH_LONG).show();
			Log.e("JSON Exception", e.toString());
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int id) {
		Toast.makeText(this, getString(R.string.setting_wallpaper),
				Toast.LENGTH_LONG).show();
		new GetJSONTask(
				"http://wcischeduleapp.com/photo-wallpaper/get_json.php", this)
				.execute();
		return Service.START_NOT_STICKY;
	}

}
