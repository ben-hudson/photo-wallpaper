package photowallpaper.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class GetJSONTask extends AsyncTask<Object, Object, JSONObject> {

	private String url;
	private AsyncTaskListener listener = null;

	public GetJSONTask(String url, AsyncTaskListener listener) {
		super();
		this.url = url;
		this.listener = listener;
	}

	@Override
	protected JSONObject doInBackground(Object... arg) {
		InputStream is = null;
		String json = "";
		JSONObject jObj = new JSONObject();

		try {
			URLConnection urlConnection = new URL(url).openConnection();
			is = urlConnection.getInputStream();
		} catch (MalformedURLException e) {
			Log.e("Malformed URL Exception", e.toString());
		} catch (IOException e) {
			Log.e("IO Exception", e.toString());
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (UnsupportedEncodingException e) {
			Log.e("Unsupported Encoding Exception", e.toString());
		} catch (IOException e) {
			Log.e("IO Exception", e.toString());
		} catch (NullPointerException e) {
			Log.e("Null Pointer Exception", e.toString());
		}

		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("JSON Exception", e.toString());
		} catch (NullPointerException e) {
			Log.e("Null Pointer Exception", e.toString());
		}

		return jObj;
	}

	@Override
	protected void onPostExecute(JSONObject jObj) {
		if (listener != null) {
			listener.onJSONTaskResponse(jObj);
		}
	}

}
