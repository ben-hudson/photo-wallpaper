package photowallpaper.android;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DescriptionActivity extends ActionBarActivity implements
		AsyncTaskListener, FragmentInflateListener {

	public static class PlaceholderFragment extends Fragment {

		private FragmentInflateListener listener = null;

		public PlaceholderFragment() {
			// Do nothing
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			listener = (FragmentInflateListener) activity;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View root = inflater.inflate(R.layout.fragment_description,
					container, false);
			if (listener != null) {
				listener.onFragmentInflate(root);
			}
			return root;
		}
	}

	private TextView titleView;
	private TextView descriptionView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_description);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		new GetJSONTask(
				"http://wcischeduleapp.com/photo-wallpaper/get_json.php", this)
				.execute();
	}

	@Override
	public void onFragmentInflate(View root) {
		titleView = (TextView) root.findViewById(R.id.description_title_view);
		descriptionView = (TextView) root
				.findViewById(R.id.description_description_view);
		descriptionView.setMovementMethod(LinkMovementMethod.getInstance());
	}

	@Override
	public void onImageTaskRepsonse(Bitmap bitmap) {
		// Do nothing
	}

	@Override
	public void onJSONTaskResponse(JSONObject jObj) {
		try {
			titleView.setText(jObj.getString("title"));
			descriptionView
					.setText(Html.fromHtml(jObj.getString("description")));
		} catch (JSONException e) {
			titleView.setText(R.string.error_title);
			descriptionView.setText(R.string.error_description);
		}
	}

}
