package photowallpaper.android;

import android.view.View;

public interface FragmentInflateListener {

	void onFragmentInflate(View root);

}
