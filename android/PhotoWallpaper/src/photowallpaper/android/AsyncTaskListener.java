package photowallpaper.android;

import org.json.JSONObject;

import android.graphics.Bitmap;

public interface AsyncTaskListener {

	void onImageTaskRepsonse(Bitmap bitmap);

	void onJSONTaskResponse(JSONObject jObj);

}
