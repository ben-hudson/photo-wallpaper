package photowallpaper.android;

import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.WallpaperManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements
		AsyncTaskListener, FragmentInflateListener {

	public static class PlaceholderFragment extends Fragment {

		private FragmentInflateListener listener = null;

		public PlaceholderFragment() {
			// Do nothing
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			listener = (FragmentInflateListener) activity;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View root = inflater.inflate(R.layout.fragment_main, container,
					false);
			if (listener != null) {
				listener.onFragmentInflate(root);
			}
			return root;
		}
	}

	private SharedPreferences settings;
	private JSONObject response;

	private ImageView photoView;
	private ProgressBar spinner;
	private TextView titleView;
	private TextView descriptionView;

	public void hideOverlay(View view) {
		if (titleView.getVisibility() == View.INVISIBLE) {
			titleView.setVisibility(View.VISIBLE);
			descriptionView.setVisibility(View.VISIBLE);
		} else {
			titleView.setVisibility(View.INVISIBLE);
			descriptionView.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		settings = PreferenceManager.getDefaultSharedPreferences(this);

		new GetJSONTask(
				"http://wcischeduleapp.com/photo-wallpaper/get_json.php", this)
				.execute();

		if (settings.getBoolean("setup", true)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					new ContextThemeWrapper(this, android.R.style.Theme_Holo));
			builder.setTitle(R.string.dialog_title);
			builder.setMessage(R.string.dialog_text);
			builder.setCancelable(false);
			builder.setPositiveButton(R.string.yes,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							AlarmManager manager = (AlarmManager) MainActivity.this
									.getSystemService(Context.ALARM_SERVICE);
							Intent receiver = new Intent(MainActivity.this,
									AlarmReceiver.class);
							PendingIntent alarm = PendingIntent.getBroadcast(
									MainActivity.this, 0, receiver,
									PendingIntent.FLAG_UPDATE_CURRENT);
							Calendar trigger = Calendar.getInstance();
							trigger.set(Calendar.HOUR_OF_DAY, 0);
							trigger.set(Calendar.MINUTE, 30);
							manager.setInexactRepeating(AlarmManager.RTC,
									trigger.getTimeInMillis(),
									AlarmManager.INTERVAL_DAY, alarm);
							Log.d("Alert Dialog", "Started alarm");

							SharedPreferences.Editor editor = settings.edit();
							editor.putBoolean("toggle", true);
							editor.commit();
							invalidateOptionsMenu();
						}
					});
			builder.setNegativeButton(R.string.no,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							SharedPreferences.Editor editor = settings.edit();
							editor.putBoolean("toggle", false);
							editor.commit();
							invalidateOptionsMenu();
						}
					});
			builder.create().show();

			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean("setup", false);
			editor.commit();
		} else if (settings.getBoolean("toggle", false)) {
			AlarmManager manager = (AlarmManager) this
					.getSystemService(Context.ALARM_SERVICE);
			Intent receiver = new Intent(this, AlarmReceiver.class);
			PendingIntent alarm = PendingIntent.getBroadcast(this, 0, receiver,
					PendingIntent.FLAG_UPDATE_CURRENT);
			Calendar trigger = Calendar.getInstance();
			trigger.add(Calendar.DATE, 1);
			trigger.set(Calendar.HOUR_OF_DAY, 0);
			trigger.set(Calendar.MINUTE, 30);
			manager.setInexactRepeating(AlarmManager.RTC,
					trigger.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarm);
			Log.d("Main Activity", "Started alarm");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (settings.getBoolean("toggle", false)) {
			getMenuInflater().inflate(R.menu.on, menu);
		} else {
			getMenuInflater().inflate(R.menu.off, menu);
		}
		return true;
	}

	@Override
	public void onFragmentInflate(View root) {
		photoView = (ImageView) root.findViewById(R.id.photo_view);
		spinner = (ProgressBar) root.findViewById(R.id.spinner);
		titleView = (TextView) root.findViewById(R.id.title_view);
		descriptionView = (TextView) root.findViewById(R.id.description_view);
		descriptionView.setMovementMethod(new LinkMovementMethod() {
			@Override
			public boolean onTouchEvent(TextView widget, Spannable buffer,
					MotionEvent event) {
				try {
					return super.onTouchEvent(widget, buffer, event);
				} catch (ActivityNotFoundException e) {
					Intent intent = new Intent(MainActivity.this,
							DescriptionActivity.class);
					MainActivity.this.startActivity(intent);
					Log.e("Activity Not Found Exception", e.toString());
					return true;
				}
			}
		});
	}

	@Override
	public void onImageTaskRepsonse(Bitmap bitmap) {
		try {
			spinner.setVisibility(View.GONE);
			photoView.setImageBitmap(bitmap);
			titleView.setText(response.getString("title"));
			descriptionView.setText(Html.fromHtml(response
					.getString("paragraph")));
		} catch (Exception e) {
			titleView.setText(R.string.error_title);
			descriptionView.setText(R.string.error_description);
			Log.e("Exception", e.toString());
		}
	}

	@Override
	public void onJSONTaskResponse(JSONObject jObj) {
		response = jObj;
		WallpaperManager manager = WallpaperManager.getInstance(this);
		Point size = new Point(manager.getDesiredMinimumWidth(),
				manager.getDesiredMinimumHeight());
		try {
			new GetImageTask(response.getString("link") + size.x + "x" + size.y
					+ ".jpg", this).execute();
		} catch (JSONException e) {
			spinner.setVisibility(View.GONE);
			titleView.setText(R.string.error_title);
			descriptionView.setText(R.string.error_description);
			Log.e("JSON Exception", e.toString());
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_toggle) {
			SharedPreferences.Editor editor = settings.edit();
			AlarmManager manager = (AlarmManager) this
					.getSystemService(Context.ALARM_SERVICE);
			Intent receiver = new Intent(this, AlarmReceiver.class);
			PendingIntent alarm = PendingIntent.getBroadcast(this, 0, receiver,
					PendingIntent.FLAG_UPDATE_CURRENT);
			if (settings.getBoolean("toggle", false)) {
				manager.cancel(alarm);
				Log.d("Main Activity", "Cancelled alarm");

				editor.putBoolean("toggle", false);
			} else {
				Calendar trigger = Calendar.getInstance();
				trigger.set(Calendar.HOUR_OF_DAY, 0);
				trigger.set(Calendar.MINUTE, 30);
				manager.setInexactRepeating(AlarmManager.RTC,
						trigger.getTimeInMillis(), AlarmManager.INTERVAL_DAY,
						alarm);
				Log.d("Main Activity", "Started alarm");

				editor.putBoolean("toggle", true);
			}
			editor.commit();
			invalidateOptionsMenu();
			return true;
		} else if (id == R.id.action_refresh) {
			spinner.setVisibility(View.VISIBLE);
			new GetJSONTask(
					"http://wcischeduleapp.com/photo-wallpaper/get_json.php",
					this).execute();
		} else if (id == R.id.action_set_wallpaper) {
			Intent intent = new Intent(this, WallpaperService.class);
			startService(intent);
			return true;
		} else if (id == R.id.action_visit_feed) {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri
					.parse("http://feeds.nationalgeographic.com/ng/photography/photo-of-the-day/"));
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
