package photowallpaper.android;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(context);

		if (settings.getBoolean("toggle", false)) {
			AlarmManager manager = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			Intent receiver = new Intent(context, AlarmReceiver.class);
			PendingIntent alarm = PendingIntent.getBroadcast(context, 0,
					receiver, PendingIntent.FLAG_UPDATE_CURRENT);
			Calendar trigger = Calendar.getInstance();
			trigger.set(Calendar.HOUR_OF_DAY, 0);
			trigger.set(Calendar.MINUTE, 30);
			manager.setInexactRepeating(AlarmManager.RTC,
					trigger.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarm);
			Log.d("Boot Receiver", "Started alarm");
		}
	}

}
