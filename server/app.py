import os
import requests

from flask import Flask
from flask_restful import Api, Resource
from xml.etree import ElementTree
from BeautifulSoup import BeautifulSoup

app = Flask(__name__)
api = Api(app)

class RSSParser(Resource):
    def get(self):
        rss = requests.get('http://feeds.nationalgeographic.com/ng/photography/photo-of-the-day/').text
        item = ElementTree.fromstring(rss.encode('utf-8')).find('channel').find('item')

        title = item.find('title').text
        description = item.find('description').text
        link = item.find('enclosure').attrib['url']

        return {'title': title, 'description': description, 'link': link}

class HTMLParser(Resource):
    def get(self):
        response = requests.get('http://www.nationalgeographic.com/photography/photo-of-the-day/')
        soup = BeautifulSoup(response.text)

        title = soup.find("meta", {"property": "og:title"})
        description = soup.find("meta", {"property": "og:description"})
        image = soup.find("meta", {"property": "og:image"})

        return {'title': title['content'], 'description': description['content'], 'link': image['content']}

api.add_resource(RSSParser, '/')
api.add_resource(HTMLParser, '/v2/')

if __name__ == '__main__':
    port = os.environ.get('PORT', '5000')
    app.run(host='0.0.0.0', port=int(port))
