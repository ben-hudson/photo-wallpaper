import os
import json
import datetime
import time
import urllib2
import webbrowser

from ctypes.windll import user32

width = str(user32.GetSystemMetrics(0))
height = str(user32.GetSystemMetrics(1))

path = os.path.join(os.path.expanduser('~'), 'Pictures\National Geographic Photo of the Day')
if not os.path.exists(path):
    os.makedirs(path)

success = False
while not success:
    try:
        response = json.loads(urllib2.urlopen('http://photo-wallpaper.herokuapp.com/').read())
        src = response['link'][:-11] + width + 'x' + height + '.jpg'
        dst = os.path.join(path, 'image' + str(datetime.datetime.today().weekday()) + '.jpg')

        with open(dst, mode='wb') as image:
            image.write(urllib2.urlopen(src).read())
            image.close()

        user32.SystemParametersInfoA(20, 0, dst)
        webbrowser.open_new('http://feeds.nationalgeographic.com/ng/photography/photo-of-the-day/')
        success = True
    except:
        time.sleep(15)
